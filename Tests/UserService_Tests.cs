﻿using Microsoft.Extensions.DependencyInjection;
using Shouldly;
using System;
using System.Threading.Tasks;
using Xunit;
using Сonsumer.IServices;

namespace Tests
{
    public class UserService_Tests : BaseTest
    {
        private IUserService _userService;

        public UserService_Tests()
        {
            _userService = base.ServiceProvider.GetRequiredService<IUserService>();
        }

        [Fact]
        public async Task GetFriends()
        {
            var user = await _userService.GetUserAsync(1);
            user.ShouldNotBeNull("user is null or empty");
            user.uid.ShouldBeGreaterThan(0, "uid was less then 1");

        }
    }
}
