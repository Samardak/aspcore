﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Сonsumer.Models;

namespace Сonsumer.IServices
{
    public interface IUserService
    {
        Task<User> GetUserAsync(long uid);
    }
}
