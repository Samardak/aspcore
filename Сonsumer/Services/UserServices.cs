﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Сonsumer.IService;
using Сonsumer.IServices;
using Сonsumer.Models;

namespace Сonsumer.Services
{
    public class UserService : IUserService
    {
        private IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public async Task<User> GetUserAsync(long uid)
        {
            return await _userRepository.GetUser(uid);
        }
    }
}
