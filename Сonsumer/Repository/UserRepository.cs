﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Сonsumer.IService;
using Сonsumer.Models;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Options;

namespace Сonsumer.Service
{
    public class UserRepository : IUserRepository
    {
        private AppSettings _appSettings { get; set; }
        public UserRepository(IOptions<AppSettings> options)
        {
            _appSettings = options.Value;
        }
        public IDbConnection Connection
        {
            get
            {
                //return new MySqlConnection("Server=hofqa.dublin.local;Database=facebook;Uid=root;Pwd=ASDcxz111;Maximum Pool Size=150;SslMode=none");
                return new MySqlConnection(_appSettings.SQLConnection);
            }
        }

        public async Task<User> GetUser(long id)
        {
            IDbConnection dbConnection = null;
            try
            {
                dbConnection = Connection;
                
                string sQuery = "SELECT uid, first_name, last_name FROM facebook.user WHERE uid = @ID";
                dbConnection.Open();
                var result = await dbConnection.QueryAsync<User>(sQuery, new { ID = id });
                return result.FirstOrDefault();
                
            }
            catch (System.Exception ex)
            {
                return null;                
            }
            finally
            {
                dbConnection?.Close();
            }
        }
    }
}
