﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Сonsumer.Models;

namespace Сonsumer.IService
{
    public interface IUserRepository
    {
        Task<User> GetUser(long id);
    }
}
