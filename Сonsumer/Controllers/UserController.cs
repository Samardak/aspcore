﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Сonsumer.IService;
using Сonsumer.IServices;

namespace Сonsumer.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private AppSettings _options;
        private IUserService _userService;
        public UserController(IOptions<AppSettings> options, IUserService userService)
        {
            _userService = userService;
            _options = options.Value;
        }
        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { _options.TestVariable, "value2" };
        }

        /// <summary>
        /// HTTP Method (you can describe as you want)
        /// </summary>
        /// <param name="uid">user id</param>
        /// <returns>uid + first name + last name</returns>
        [HttpGet("{uid}")]
        public async Task<string> GetAsync(int uid)
        {
            var user = await _userService.GetUserAsync(uid);
            return $"{user?.uid} {user?.first_name} {user?.last_name}";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        [HttpPost]
        [Route("MyAction")]
        public async Task<string> RegisterAndLogin([FromBody]string input)
        {
            return input;
        }


        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
